package me.teacms.dao;

import me.teacms.entity.Multimedia;

import java.util.List;

public interface MultimediaMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Multimedia record);

    int insertSelective(Multimedia record);

    Multimedia selectByPrimaryKey(Integer id);

    List<Multimedia> findAllImg(Multimedia record);

    int updateByPrimaryKeySelective(Multimedia record);

    int updateByPrimaryKey(Multimedia record);
}