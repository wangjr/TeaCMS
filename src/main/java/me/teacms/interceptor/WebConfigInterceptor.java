package me.teacms.interceptor;

import me.teacms.common.init.InitConfig;
import me.teacms.dao.CommonMapper;
import me.teacms.dao.IndexDao;
import me.teacms.entity.Category;
import me.teacms.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/28
 * Time: 19:26
 * Describe: 网站配置 和 通用查询
 */
public class WebConfigInterceptor implements HandlerInterceptor {

    @Autowired
    private CommonMapper commonMapper;
    @Autowired
    private IndexDao indexDao;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        List<Category> allCategory = commonMapper.findAllCategory();
        List<Tag> allTag = commonMapper.findAllTag();
        modelAndView.addObject("webConfig", InitConfig.getWebConfig());
        modelAndView.addObject("allCategory", allCategory);
        modelAndView.addObject("allTag", allTag);
        modelAndView.addObject("blogroll", indexDao.findBlogroll());
        modelAndView.addObject("newArticleTop6", indexDao.findNewArticleTop6());
        modelAndView.addObject("viewArticleTop6", indexDao.findViewArticleTop6());
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
