package me.teacms.controller;

import me.teacms.entity.Users;
import me.teacms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

/**
 * Created by XiaoBingBy on 2017/2/5.
 */
@Controller
public class TeaCMS {

    @Autowired
    private UserService allUser;

    @ResponseBody
    @RequestMapping(value = "/index")
    public Users index1() {

        Users allUser = this.allUser.findAllUser();

        return allUser;
    }

    @RequestMapping(value = "/test")
    public ModelAndView test() {
        ModelAndView view = new ModelAndView();
        view.setViewName("blank");

        return view;
    }

    @RequestMapping(value = "/index1.html")
    public String t() {

        return "_themes/TeaCMS/index";
    }

}
