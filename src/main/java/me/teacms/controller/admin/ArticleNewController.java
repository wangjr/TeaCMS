package me.teacms.controller.admin;

import com.github.pagehelper.PageInfo;
import me.teacms.common.result.JsonResult;
import me.teacms.dao.CommonMapper;
import me.teacms.dao.MultimediaMapper;
import me.teacms.entity.*;
import me.teacms.entity.vo.ArticleNewVo;
import me.teacms.service.ArticleNewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/20
 * Time: 23:57
 * Describe: 创建文章
 */
@Controller
@RequestMapping(value = "/admin")
public class ArticleNewController {

    @Autowired
    private CommonMapper commonMapper;
    @Autowired
    private ArticleNewService articleNewService;
    @Autowired
    private MultimediaMapper multimediaMapper;

    /**
     * 新建文章视图
     * @return
     */
    @RequestMapping(value = "/article-new.html")
    public String articleNew(ModelMap modelMap) {

        List<Category> allCategory = commonMapper.findAllCategory();
        List<Tag> allTag = commonMapper.findAllTag();

        /**
         * 插入图片
         */
        List<Multimedia> allImg = multimediaMapper.findAllImg(null);
        PageInfo allImgInfo = new PageInfo(allImg);

        modelMap.put("allCategory", allCategory);
        modelMap.put("allTag", allTag);
        modelMap.put("allImgInfo", allImgInfo.getList());

        return "_admin/article/article_new";
    }

    /**
     * 添加文章
     * @param articleNewVo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/addarticle")
    public JsonResult addArticle(HttpSession session, ArticleNewVo articleNewVo) {

        /**
         * 添加用户信息
         */
        User user = (User) session.getAttribute("user");
        articleNewVo.setArticleAuthor(user.getId());
        JsonResult jsonResult = articleNewService.addArticle(articleNewVo);

        return jsonResult;
    }

}
