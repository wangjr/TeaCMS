package me.teacms.common.init;

import me.teacms.dao.ArticleAllDao;
import me.teacms.dao.WebConfigMapper;
import me.teacms.entity.WebConfig;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/5
 * Time: 15:40
 * Describe: 网站配置初始化
 */
public class InitConfig {

    @Autowired
    private WebConfigMapper webConfigMapper;

    private static WebConfig webConfig;

    public void initWebConfig() {
        webConfig = webConfigMapper.selectByPrimaryKey(1);
        System.err.println(" webConfig = webConfigMapper.selectByPrimaryKey(1);");
    }

    public static WebConfig getWebConfig() {
        return webConfig;
    }

    public static void setWebConfig(WebConfig webConfig) {
        InitConfig.webConfig = webConfig;
    }
}
