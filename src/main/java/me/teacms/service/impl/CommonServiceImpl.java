package me.teacms.service.impl;

import me.teacms.dao.CommonMapper;
import me.teacms.entity.Category;
import me.teacms.entity.Tag;
import me.teacms.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/22
 * Time: 16:46
 * Describe: 公共业务方法
 */
@Service
public class CommonServiceImpl implements CommonService {

    @Autowired
    private CommonMapper commonMapper;

    /**
     * 查询所有分类
     * @return
     */
    @Override
    public List<Category> findAllCategory() {
        return commonMapper.findAllCategory();
    }

    /**
     * 查询所有标签
     * @return
     */
    @Override
    public List<Tag> findAllTag() {
        return commonMapper.findAllTag();
    }
}
