package me.teacms.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import me.teacms.common.init.InitConfig;
import me.teacms.dao.CategoryAndTagDao;
import me.teacms.entity.vo.ArticleAllVo;
import me.teacms.service.CategoryAndTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/8
 * Time: 6:43
 * Describe: 文章分类和标签业务方法
 */
@Service
public class CategoryAndTagServiceImpl implements CategoryAndTagService {

    @Autowired
    private CategoryAndTagDao categoryAndTagDao;

    /**
     * 查询所有文章分类
     * @param pageNum
     * @param alias
     * @return
     */
    @Override
    public PageInfo findAllCategoryArticle(Integer pageNum, String alias) {

        PageHelper.startPage(pageNum, InitConfig.getWebConfig().getPagesize());
        List<ArticleAllVo> allCategoryArticle = categoryAndTagDao.findAllCategoryArticle(alias);
        PageInfo pageInfo = new PageInfo(allCategoryArticle);

        return pageInfo;
    }

    /**
     * @param pageNum
     * 查询所有标签分类
     * @param alias
     * @return
     */
    @Override
    public PageInfo findAllTagArticle(Integer pageNum, String alias) {

        PageHelper.startPage(pageNum, InitConfig.getWebConfig().getPagesize());
        List<ArticleAllVo> allTagArticle = categoryAndTagDao.findAllTagArticle(alias);
        PageInfo pageInfo = new PageInfo(allTagArticle);

        return pageInfo;
    }
}
