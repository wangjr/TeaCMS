package me.teacms.service;

import me.teacms.common.result.JsonResult;
import me.teacms.entity.vo.BannerVo;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/12
 * Time: 14:19
 * Describe:
 */
public interface BannerService {

    /**
     * 查找所有Banner
     * @return
     */
    public JsonResult findAllBanner();

    /**
     *  Banner 查找要添加所有Banner的文章
     * @return
     */
    List<BannerVo> findAllBannerArticle();

    /**
     * 删除Banner
     * @param id
     * @return
     */
    public Integer deleteBanner(Integer id);

    /**
     * 添加 Banner
     * @param bannerVo
     * @return
     */
    public Integer addBanner(BannerVo bannerVo);

    /**
     * 更新 Banner
     * @param bannerVo
     * @return
     */
    public Integer updateBanner(BannerVo bannerVo);

}
