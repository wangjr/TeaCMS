package me.teacms.entity.vo;

/**
 * 主题信息
 * @author XiaoBingBy
 *
 */
public class Theme {

	private String themeName;

	private String themePath;
	
	private boolean state;

	private String screenshot;

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}


	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}

	public String getThemePath() {
		return themePath;
	}

	public void setThemePath(String themePath) {
		this.themePath = themePath;
	}

	public String getScreenshot() {
		return screenshot;
	}

	public void setScreenshot(String screenshot) {
		this.screenshot = screenshot;
	}

}
