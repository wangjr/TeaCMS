package me.teacms.entity.vo;

import me.teacms.entity.Blogroll;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/12
 * Time: 14:10
 * Describe: BannerVo
 */
public class BannerVo extends Blogroll {

    private String articleTitle;

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }
}
