<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>TeaCMS-后台管理</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- pace -->
  <script src="/static/plugins/pace-0.7.5/pace.js"></script>
  <link href="/static/plugins/pace-0.7.5/themes/black/pace-theme-center-circle.css" rel="stylesheet" />
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="/static/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/static/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/static/plugins/animate/animate.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="/static/css/skins/my_all-skins.css">
  <!-- MyAdminLTE.css -->
  <link rel="stylesheet" href="/static/css/MyAdminLTE.css">
  <!-- Lobibox -->
  <link rel="stylesheet" href="/static/plugins/lobibox/css/lobibox.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
<#import "/_admin/lib/sidebar_templet.ftl" as sidebar_templet>
<!-- Site wrapper -->
<div class="wrapper">
  <#-- header -->
  <#include "header.ftl">
    <#-- sidebar -->
  <@sidebar_templet.sidebar "main"></@sidebar_templet.sidebar>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        仪表盘
        <small></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Web 执行统计</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="height: 400px">
                        <div id="webConfig" style="width: 100%;height:100%;"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">数据统计</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="height: 400px">
                        <div id="main" style="width: 100%;height:100%;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">CPU</h3>
                        &nbsp;
                        <button type="button" class="btn btn-default">天</button>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <img id="CPU" src="/monitoring?width=960&height=400&graph=cpu" class="img-responsive" style="width: 100%" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">内存</h3>
                        &nbsp;
                        <button type="button" class="btn btn-default">天</button>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <img id="RAM" src="/monitoring?width=960&height=400&graph=usedMemory" class="img-responsive" style="width: 100%" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <#include "footer.ftl">

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="/static/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/static/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/static/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/static/js/demo.js"></script>
<script src="/static/plugins/echarts/echarts.min.js"></script>
<script src="/static/plugins/lobibox/js/lobibox.min.js"></script>
<script src="/static/js/common.js"></script>
<script>
    function deletMSG(type , msg) {
        Lobibox.notify(type, {
            size: 'mini',
            soundPath: '/static/plugins/lobibox/sounds/',
            position: 'center bottom',
            msg: msg
        });
    }
      $(function () {
          var dataChart = [];
          ajaxSubmitForm("/druid/weburi.json", null,
                  function (data) {
                    for (i = 0; i<data.Content.length; i++) {
                        /*console.log(data.Content[i].URI);
                        console.log(data.Content[i].RequestCount);*/
                        if (data.Content[i].URI.indexOf("/admin/") == -1 && data.Content[i].URI.indexOf("/static/") == -1 && data.Content[i].URI.indexOf("/views/") == -1  && data.Content[i].URI.indexOf("/monitoring") == -1) {
                            dataChart.push({value:data.Content[i].RequestCount, name:data.Content[i].URI})
                        }
                    }
                  },
                  function (data) {

                  }
          );
          var myChart = echarts.init(document.getElementById('main'));


          option = {
              backgroundColor: '#fff',

              title: {
                  text: 'Url访问统计',
                  left: 'center',
                  top: 20,
                  textStyle: {
                      color: '#3c8dbc'
                  }
              },

              tooltip : {
                  trigger: 'item',
                  formatter: "{a} <br/>{b} : {c} ({d}%)"
              },

              visualMap: {
                  show: false,
                  min: 80,
                  max: 600,
                  inRange: {
                      colorLightness: [0, 1]
                  }
              },
              series : [
                  {
                      name:'访问来源',
                      type:'pie',
                      radius : '80%',
                      center: ['50%', '50%'],
                      data:dataChart.sort(function (a, b) { return a.value - b.value}),
                      roseType: 'angle',
                      label: {
                          normal: {
                              textStyle: {
                                  color: '#3c8dbc'
                              }
                          }
                      },
                      labelLine: {
                          normal: {
                              lineStyle: {
                                  color: '#3c8dbc'
                              },
                              smooth: 0.2,
                              length: 10,
                              length2: 20
                          }
                      },
                      itemStyle: {
                          normal: {
                              color: '#3c8dbc',
                              shadowBlur: 0,
                              shadowColor: 'rgba(0, 0, 0, 0.5)'
                          }
                      },

                      animationType: 'scale',
                      animationEasing: 'elasticOut',
                      animationDelay: function (idx) {
                          return Math.random() * 200;
                      }
                  }
              ]
          };
          // 使用刚指定的配置项和数据显示图表。
          myChart.setOption(option);

          var webConfigChart = echarts.init(document.getElementById('webConfig'));

          var webConfigData = [0, 0, 0, 0, 0];

          ajaxSubmitForm("/druid/webapp.json", null,
                  function (data) {
                      console.log(data.Content);
                      webConfigData[0] = data.Content[0].ConcurrentMax;
                      webConfigData[1] = data.Content[0].RequestCount;
                      webConfigData[2] = data.Content[0].JdbcExecuteCount;
                      webConfigData[3] = data.Content[0].JdbcExecuteTimeMillis;
                      webConfigData[4] = data.Content[0].JdbcFetchRowCount;
                  },
                  function (data) {

                  }
          );

          // 指定图表的配置项和数据
          var option1 = {
              color: ['#3398DB'],
              tooltip : {
                  trigger: 'axis',
                  axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                      type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                  }
              },
              grid: {
                  left: '3%',
                  right: '4%',
                  bottom: '3%',
                  containLabel: true
              },
              xAxis : [
                  {
                      type : 'category',
                      data : ['最大并发', '请求次数', 'Jdbc执行数', 'Jdbc时间', '读取行数'],
                      axisTick: {
                          alignWithLabel: true
                      }
                  }
              ],
              yAxis : [
                  {
                      type : 'value'
                  }
              ],
              series : [
                  {
                      name:'--',
                      type:'bar',
                      barWidth: '60%',
                      data:webConfigData
                  }
              ]
          };
          // 使用刚指定的配置项和数据显示图表。
          webConfigChart.setOption(option1);
      });
      $('#alertPass').submit(function () {
          ajaxSubmitForm("/admin/alertpass", $(this).serialize(),
                  function (data) {
                        if (data.success) {
                            deletMSG('success', data.msg);
                        } else {
                            deletMSG('error', data.msg);
                        }
                  },
                  function (data) {
                      deletMSG('error', '网络错误，请刷新!');
                  }
          );

          return false; //阻止表单默认提交
      });
</script>
</body>
</html>
