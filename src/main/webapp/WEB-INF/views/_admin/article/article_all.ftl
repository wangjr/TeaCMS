<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TeaCMS-后台管理</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="/static/plugins/pace-0.7.5/pace.js"></script>
    <link href="/static/plugins/pace-0.7.5/themes/black/pace-theme-center-circle.css" rel="stylesheet" />
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Lobibox -->
    <link rel="stylesheet" href="/static/plugins/lobibox/css/lobibox.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/static/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/static/css/skins/my_all-skins.css">
    <!-- MyAdminLTE.css -->
    <link rel="stylesheet" href="/static/css/MyAdminLTE.css">

    <style>
        .table-border {
            border: 1px solid #cccccc;
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
<#import "/_admin/lib/sidebar_templet.ftl" as sidebar_templet>
<!-- Site wrapper -->
<div class="wrapper">
<#-- header -->
<#include "/_admin/header.ftl">
<#-- sidebar -->
<@sidebar_templet.sidebar "article-all"></@sidebar_templet.sidebar>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                所有文章
                <small>All Articles</small>
                <a href="/admin/all-user.html" class="btn btn-default">写文章</a>
            </h1>
        </section>
        <!-- Main content -->
        <section class="content" id="app">
            <div id="info">

            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#all-article" data-toggle="tab" aria-expanded="true" v-on:click="switchView(0)">全部</a></li>
                            <li class=""><a href="#recyclebin" data-toggle="tab" aria-expanded="false"  v-on:click="switchView(1)">回收站</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="box-tools pull-right">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" id="search" class="form-control pull-right" placeholder="请输入文章名">

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default" v-on:click="getAllArticleInfo()" v-if="view == 0"><i class="fa fa-search"></i></button>
                                        <button type="submit" class="btn btn-default" v-on:click="getAllTrashCanArticle()" v-if="view == 1"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane active" id="all-article">
                                <table class="table table-hover" style="margin-bottom: 0">
                                    <thead>
                                    <tr>
                                        <th># ID1</th>
                                        <th>标题</th>
                                        <th>作者</th>
                                        <th>文章分类</th>
                                        <th>文章标签</th>
                                        <th>日期</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="item in pageInfoAllArticle.list">
                                            <td v-text="item.id"></td>
                                            <td v-text="item.articleTitle"></td>
                                            <td v-text="item.userNicename"></td>
                                            <td><span  v-for="a in item.categories">{{a.name}}<br></span></td>
                                            <td><span  v-for="a in item.tags">{{a.name}}<br></span></td>
                                            <td>{{item.updateDate | moment}}</td>
                                            <td>
                                                <#--<button class="btn btn-xs btn-primary" v-on:click="editArticle(item.id)">编辑</button>-->
                                                <a :href="'/admin/article-edit.html?id='+item.id+'&edit=edit'" class="btn btn-xs btn-primary">编辑</a>
                                                <button class="btn btn-xs btn-danger" v-on:click="deleteLogicArticle(item.id)">删除</button>
                                            </td>
                                        </tr>
                                        <tr v-if="pageInfoAllArticle.list == 0">
                                            <td colspan="7" align="center">暂无数据</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="box-footer clearfix">
                                    <ul class="pagination no-margin pull-right" v-if="pageInfoAllArticle.list != 0">
                                        <li v-if="!pageInfoAllArticle.isFirstPage"><a href="javscript:;" v-on:click="getPageAllArticle(pageInfoAllArticle.prePage)">«</a></li>
                                        <li v-if="pageInfoAllArticle.isFirstPage" class="disabled"><a href="javscript:;">«</a></li>
                                        <li v-for="item in pageInfoAllArticle.navigatepageNums" :class="item == pageInfoAllArticle.pageNum? 'active':''"><a href="javascript:void(0);" v-text="item" v-on:click="getPageAllArticle(item)"></a></li>
                                        <li v-if="!pageInfoAllArticle.isLastPage"><a href="javscript:;" v-on:click="getPageAllArticle(pageInfoAllArticle.nextPage)">»</a><b></b></li>
                                        <li v-if="pageInfoAllArticle.isLastPage" class="disabled"><a href="javscript:;">»</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="tab-pane" id="recyclebin">
                                <table class="table table-hover" style="margin-bottom: 0">
                                    <thead>
                                        <tr>
                                            <th># ID3</th>
                                            <th>标题</th>
                                            <th>作者</th>
                                            <th>文章分类</th>
                                            <th>文章标签</th>
                                            <th>日期</th>
                                            <th>操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="item in pageInfoAllTrashCanArticle.list">
                                            <td v-text="item.id"></td>
                                            <td v-text="item.articleTitle"></td>
                                            <td v-text="item.userNicename"></td>
                                            <td><span  v-for="a in item.categories">{{a.name}}<br></span></td>
                                            <td><span  v-for="a in item.tags">{{a.name}}<br></span></td>
                                            <td>{{item.updateDate | moment}}</td>
                                            <td>
                                                <button class="btn btn-xs btn-primary" v-on:click="recoverArticle(item.id)">还原</button>
                                                <button class="btn btn-xs btn-danger" v-on:click="deleteArticle(item.id)">彻底删除</button>
                                            </td>
                                        </tr>
                                        <tr  v-if="pageInfoAllTrashCanArticle.list == 0">
                                            <td colspan="7" align="center">暂无数据</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="box-footer clearfix" v-if="pageInfoAllTrashCanArticle.list != 0">
                                    <ul class="pagination no-margin pull-right">
                                        <li v-if="!pageInfoAllTrashCanArticle.isFirstPage"><a href="javscript:;" v-on:click="getPageAllTrashCanArticle(pageInfoAllTrashCanArticle.prePage)">«</a></li>
                                        <li v-if="pageInfoAllTrashCanArticle.isFirstPage" class="disabled"><a href="javscript:;">«</a></li>
                                        <li v-for="item in pageInfoAllTrashCanArticle.navigatepageNums" :class="item == pageInfoAllTrashCanArticle.pageNum? 'active':''"><a href="javascript:void(0);" v-text="item" v-on:click="getPageAllTrashCanArticle(item)"></a></li>
                                        <li v-if="!pageInfoAllTrashCanArticle.isLastPage"><a href="javscript:;" v-on:click="getPageAllTrashCanArticle(pageInfoAllTrashCanArticle.nextPage)">»</a><b></b></li>
                                        <li v-if="pageInfoAllTrashCanArticle.isLastPage" class="disabled"><a href="javscript:;">»</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal modal-success fade" id="myModal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Success Modal</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body…</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-outline">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<#include "/_admin/footer.ftl">

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap.min.js"></script>
<!-- vue.js -->
<script src="/static/plugins/vue/vue.js"></script>
<!-- 时间格式化插件 -->
<script src="/static/plugins/moment/moment.js"></script>
<!-- SlimScroll -->
<script src="/static/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/static/plugins/fastclick/fastclick.js"></script>
<script src="/static/plugins/lobibox/js/lobibox.min.js"></script>
<!-- AdminLTE App -->
<script src="/static/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/static/js/demo.js"></script>
<script src="/static/js/common.js"></script>

</div>
<script>
    Vue.filter('moment', function (value, formatString) {
        formatString = formatString || 'YYYY-MM-DD';
        return moment(value).format(formatString);
    });

    function deletMSG(type , msg) {
        Lobibox.notify(type, {
            size: 'mini',
            soundPath: '/static/plugins/lobibox/sounds/',
            position: 'center bottom',
            msg: msg
        });
    }


    var vm = new Vue({
        el: '#app',
        data: {
            /*视图界面*/
            view: 0,
            /*所有文章搜索信息*/
            allArticleInfo: {
                pageId: 0,
                searchInfo: null
            },
            /*所有文章分页信息*/
            pageInfoAllArticle: {},
            /*所有回收站搜索信息*/
            allTrashCanArticle: {
                pageId: 0,
                searchInfo: null
            },
            /*所有回收站分页信息*/
            pageInfoAllTrashCanArticle: {}
        },
        methods: {
            /*所有文章搜索功能*/
            getAllArticleInfo: function () {
                var search = $("#search").val();
                vm.allArticleInfo.pageId = 0;
                vm.allArticleInfo.searchInfo = search;
                allArticleInfo();
            },
            /*所有文章分页功能*/
            getPageAllArticle: function(id) {
                vm.allArticleInfo.pageId = id;
                allArticleInfo();
            },
            /*所有回收站文章*/
            getAllTrashCanArticle: function () {
                var search = $("#search").val();
                vm.allTrashCanArticle.pageId = 0;
                vm.allTrashCanArticle.searchInfo = search;
                allTrashCanArticle();
            },
            /*回收站分页按钮*/
            getPageAllTrashCanArticle: function (id) {
                vm.allTrashCanArticle.pageId = id;
                allTrashCanArticle();
            },
            /*切换视图事件*/
            switchView: function (n) {
                vm.view = n;
                if (n == 0) {
                    allArticleInfo();
                } else {
                    allTrashCanArticle();
                }
            },
            /*编辑文章*/
            editArticle: function() {

            },
            /*删除文章 到 回收站*/
            deleteLogicArticle: function (id) {
                console.log(JSON.stringify({"id": id}));
                ajaxSubmit("/admin/deletelogicarticle", JSON.stringify(id),
                    function (data) {
                        if (data.obj >=1 && data.success) {
                            deletMSG('success', '删除成功!');
                            allArticleInfo();
                        } else {
                            deletMSG('error', '删除失败!');
                        }
                    },
                    function () {
                        deletMSG('error', '网络错误，请刷新!');
                    }
                );
                /*alertSuccess("info", "删除成功");*/
            },
            /*还原文章到发布文章状态中*/
            recoverArticle: function (id) {
                ajaxSubmit("/admin/recoverarticle", JSON.stringify(id),
                        function (data) {
                            if (data.obj >=1 && data.success) {
                                deletMSG('success', '还原成功!');
                                allTrashCanArticle();
                            } else {
                                deletMSG('error', '还原失败!');
                            }
                        },
                        function () {
                            deletMSG('error', '网络错误，请刷新!');
                        }
                );
            },
            /*彻底删除文章*/
            deleteArticle: function (id) {
                ajaxSubmit("/admin/deletearticle", JSON.stringify(id),
                        function (data) {
                            if (data.obj >=1 && data.success) {
                                deletMSG('success', '彻底删除成功!');
                                allTrashCanArticle();
                            } else {
                                deletMSG('error', '彻底删除失败!');
                            }
                        },
                        function () {
                            deletMSG('error', '网络错误，请刷新!');
                        }
                );
            }
        }
    });

    /*所有文章*/
    function allArticleInfo() {
        console.log(vm.allArticleInfo)
        ajaxSubmit("/admin/getallarticleinfo", JSON.stringify(vm.allArticleInfo),
            function (data) {
                vm.pageInfoAllArticle = data.obj;
            },
            function () {
                deletMSG('error', '网络错误，请刷新!');
            }
        )
    }

    /*回收站*/
    function allTrashCanArticle() {
        console.log(vm.allArticleInfo)
        ajaxSubmit("/admin/getalltrashcanarticle", JSON.stringify(vm.allTrashCanArticle),
            function (data) {
                vm.pageInfoAllTrashCanArticle = data.obj;
            },
            function () {
                deletMSG('error', '网络错误，请刷新!');
            }
        )
    }

    allArticleInfo();
</script>
</body>
</html>