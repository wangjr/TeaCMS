<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TeaCMS-后台管理</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="/static/plugins/pace-0.7.5/pace.js"></script>
    <link href="/static/plugins/pace-0.7.5/themes/black/pace-theme-center-circle.css" rel="stylesheet" />
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/static/css/AdminLTE.min.css">
    <!-- wangEditor -->
    <link rel="stylesheet" href="/static/plugins/wangEditor/css/wangEditor.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/static/css/skins/my_all-skins.css">
    <!-- MyAdminLTE.css -->
    <link rel="stylesheet" href="/static/css/MyAdminLTE.css">
    <link rel="stylesheet" href="/static/plugins/iCheck/flat/blue.css">

    <!-- Lobibox -->
    <link rel="stylesheet" href="/static/plugins/lobibox/css/lobibox.min.css">
    <style>
        @media screen and (max-width: 992px) {
            .pr {
                padding: 0;
            }
        }
        .list-group {
            margin-bottom: 0;
            margin-top: -5px;
        }
        .list-group > .list-group-item {
            border: none;
            padding: 8px 0;
            margin-left: 13px;
            margin-right: 13px;
            border-bottom: 1px dashed #b4afaf;
            margin-bottom: 0;
        }
        .list-group > .list-group-item:nth-last-child(1) {
            border-bottom: none;
        }
        .teacms-tags-list-group {
            list-style: none;
            padding-left: 0;
        }
        .teacms-tags-list-group li {
            display: inline-block;
            margin-bottom: 5px;
            border: 1px solid #ccc;
            padding: 3px;
        }
        .content-container > .content {
            min-height: 0;
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
<#import "/_admin/lib/sidebar_templet.ftl" as sidebar_templet>
<!-- Site wrapper -->
<div class="wrapper">
<#-- header -->
<#include "/_admin/header.ftl">
<#-- sidebar -->
<@sidebar_templet.sidebar "article-new"></@sidebar_templet.sidebar>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                写文章
                <small>Write An Article</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <form id="article_new_form" action="/admin/addarticle" method="post">
            <div class="col-md-9" style="padding: 0">
                <div class="box box-primary">
                    <div class="box-header with-border">
                            <input class="form-control" name="articleTitle" placeholder="请输入标题">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="editor" style="height: 550px;">
                        </div>
                    </div>
                    <input id="articleContent" name="articleContent" type="hidden">
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-3 pr">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">发布文章</h3>
                    </div>
                    <div class="box-body">
                        <div class="box-tools pull-right">
                            <div class="input-group input-group-md">
                                <input id="inputImg" type="text" name="articleImg" class="form-control pull-right" placeholder="插入文章预览图片">

                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#ModallImg">插入</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <button id="saveArticle" type="submit" class="btn btn-success"><i class="fa fa-pencil"></i> 发布</button>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">文章分类</h3>
                    </div>
                    <div class="box-body">
                        <ul class="list-group">
                            <#list allCategory as item>
                                <li class="list-group-item"> <input id="allCategory${item.id}" type="checkbox" name="articleCategories[${item_index}].categoryId" value="${item.id}"> &nbsp;<label for="allCategory${item.id}">${item.name}</label></li>
                            </#list>
                        </ul>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">文章标签</h3>
                    </div>
                    <div class="box-body">
                        <ul class="teacms-tags-list-group">
                            <#if allTag?size==0>
                                无标签&nbsp;<a href="/admin/article-tags.html" class="btn-link">添加标签</a>
                            <#else>
                                <#list allTag as item>
                                    <li><input id="allTag${item.id}" type="checkbox" name="articleTags[${item_index}].tagId" value="${item.id}"> <label for="allTag${item.id}">${item.name}</label></li>
                                </#list>
                            </#if>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            </form>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

<#include "/_admin/footer.ftl">

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<div class="modal fade" id="ModallImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    选择预览图片
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <#list allImgInfo as item>
                    <div class="col-sm-6 col-md-3">
                        <a href="#" class="thumbnail" onclick="setImg(this)">
                            <img class="img-responsive" src="${item.fileUrl}" alt="${item.fileName}">
                        </a>
                    </div>
                    </#list>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button type="button" class="btn btn-success">
                    插入
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->

<!-- jQuery 2.2.3 -->
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js" ></script>

<script src="/static/plugins/jQueryForm/jquery.form.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap.min.js"></script>
<!-- wangEditor -->
<script src="/static/plugins/wangEditor/js/wangEditor.min.js"></script>
<!-- SlimScroll -->
<script src="/static/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/static/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/static/js/app.min.js"></script>
<!-- iCheck -->
<script src="/static/plugins/iCheck/icheck.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/static/js/demo.js"></script>
<script src="/static/js/common.js"></script>
<script src="/static/plugins/lobibox/js/lobibox.min.js"></script>
<script>
    function deletMSG(type , msg) {
        Lobibox.notify(type, {
            size: 'mini',
            soundPath: '/static/plugins/lobibox/sounds/',
            position: 'center bottom',
            msg: msg
        });
    }
    var ajaxConfig = {
        type: "POST",
        dataType: "json",
        async: false
    };

    function ajaxSubmit(url, data, successFn, errorFn) {
        ajaxConfig.url = url;
        if(data != null) {
            ajaxConfig.data = data;
        };
        ajaxConfig.success = function(data) {
            successFn(data);
        };
        ajaxConfig.error = function(data) {
            errorFn(data);
        };
        $.ajax(ajaxConfig);
    };

    var editor = new wangEditor('editor');

    editor.config.uploadImgUrl = '/admin/upload';

    editor.config.uploadHeaders = {
        'enctype' : 'multipart/form-data'
    };

    editor.config.uploadImgFileName = 'myFileName';

    editor.$editorContainer.css('z-index', 0);

    // 配置 onchange 事件
    editor.onchange = function () {
        // 编辑区域内容变化时，实时打印出当前内容
        console.log(this.$txt.html());
        $("#articleContent").val(this.$txt.html());
    };

    editor.create();

    $('input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    $('#article_new_form').submit(function() {

        ajaxSubmitForm("/admin/addarticle", $(this).serialize(),
            function (data) {
                if(data.success) {
                    deletMSG('success', '发布成功!');
                }
            },
            function (data) {
                deletMSG('error', '网络错误，请刷新!');
            }
        );

        return false; //阻止表单默认提交
    });

    function setImg(obj) {
        $('#ModallImg').modal('hide')
        $("#inputImg").val(obj.getElementsByTagName('img')[0].src);
    }

</script>
</body>
</html>